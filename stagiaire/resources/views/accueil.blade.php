@extends('template')

@section('content')
    <h3 style=text-align:center>Bienvenue sur le site du lycée Pasteur Mont Roland</h3>
    <div class="container-fluid" style=background-color:grey>
        <div class="row">
            <div class="col-md-5">
                <img src="../resources/images/logo.png" class="img-responsive">
            </div>
            <div class="col-md-7">
                <p style=text-align:center>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>
        </div>
    </div>
    <div class="container-fluid" style=background-color:grey>
        <div class="col-md-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2716.238034387861!2d5.491666015489311!3d47.09440407915407!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478d4c97319a022b%3A0x10455424f077465e!2sLycee%20Pasteur%20Mont%20Roland%20P%C3%B4le%20Alternance%20Formation!5e0!3m2!1sfr!2sfr!4v1582022599955!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
@stop
